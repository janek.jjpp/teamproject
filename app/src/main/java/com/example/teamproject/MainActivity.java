package com.example.teamproject;

import android.app.AppOpsManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.Menu;
import android.view.View;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;
import com.example.teamproject.ui.Threats.Article1;
import com.example.teamproject.ui.home.HomeViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import dalvik.system.BaseDexClassLoader;

public class MainActivity extends AppCompatActivity {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private AppBarConfiguration mAppBarConfiguration;
    public static Toolbar toolbar;
    private NavController navController;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public static String FACEBOOK_COUNTER = "Facebook Counter";
    public static String YOUTUBE_COUNTER = "Youtube Counter";
    private TextView facebookView;
    private TextView youtubeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_data, R.id.nav_activity,
                R.id.nav_threats, R.id.nav_suggestion,R.id.nav_calendar, R.id.nav_about)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

//        startService(new Intent(MainActivity.this, ScreenService.class));

        sharedPreferences = getSharedPreferences("TeamProject", MODE_PRIVATE); // APP NAME
//        if (!isUsageStatsAllowed()){
//            Intent usageAccessIntent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
//            usageAccessIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(usageAccessIntent);
//            if (isUsageStatsAllowed()){
//                startService(new Intent(MainActivity.this, BackgroundService.class));
//            }
//            else {
//                Toast.makeText(getApplicationContext(), "please give access, for tracking screen time", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else{
//            startService(new Intent(MainActivity.this, BackgroundService.class));
//        }
//        facebookView = findViewById(R.id.value_one);
//        TimerTask updateView = new TimerTask() {
//            @Override
//            public void run() {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        long facebook_time = sharedPreferences.getLong(FACEBOOK_COUNTER, 0);
//                        long second = (facebook_time/1000)%60;
//                        long minutes = (facebook_time/(1000*60))%60;
//                        long hours = (facebook_time/(1000*60*60));
//                        String facebook_val = hours + "h, " + minutes + "m" + second + "s";
//                        facebookView.setText(facebook_val);
//                    }
//                });
//            }
//        };
//        Timer timer = new Timer();
//        timer.schedule(updateView, 0, 1000);
    }



    public boolean isUsageStatsAllowed(){
        try{
            PackageManager packageManager = getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager)getSystemService(APP_OPS_SERVICE);
            int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid,applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(), "cannot found user stats", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    protected void onDestroy(){
        if(isUsageStatsAllowed()){
            startService(new Intent(MainActivity.this, BackgroundService.class));
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}