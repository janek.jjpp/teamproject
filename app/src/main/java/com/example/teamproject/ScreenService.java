package com.example.teamproject;
/*
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.teamproject.ui.home.HomeFragment;

public class ScreenService extends Service {
//    private long startTimer;
//    private long endTimer;
//    private long screenOnTimeSingle;
//    private long screenOnTime;
//    private final long TIME_ERROR = 1000;
    @Override
    public void onCreate() {
        super.onCreate();
        // REGISTER RECEIVER THAT HANDLES SCREEN ON AND SCREEN OFF LOGIC
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        boolean screenOff = intent.getBooleanExtra("screen_state", false);
        if (!screenOff) {
            Toast.makeText(getBaseContext(), "Screen on", Toast.LENGTH_SHORT).show();
//            startTimer = System.currentTimeMillis();
            startCounting();
        } else {
            Toast.makeText(getBaseContext(), "Screen off", Toast.LENGTH_SHORT).show();
//            endTimer = System.currentTimeMillis();
//            screenOnTimeSingle = endTimer - startTimer;

//            if(screenOnTimeSingle < TIME_ERROR) {
//                screenOnTime += screenOnTimeSingle;
//            }
            pauseCounting();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void startCounting(){
        if(HomeFragment.lastValue != 0){
            HomeFragment.chronometer_daily.setBase(SystemClock.elapsedRealtime() + HomeFragment.lastValue);
        }
        else{
            HomeFragment.chronometer_daily.setBase(SystemClock.elapsedRealtime());
        }
        HomeFragment.chronometer_daily.start();

        //when day finish then reset Counting?
        //chronometer_daily.setBase(SystemClock.elapsedRealtime());
        //lastValue = 0;
    }
    public void pauseCounting(){
        HomeFragment.lastValue = HomeFragment.chronometer_daily.getBase() - SystemClock.elapsedRealtime();
        HomeFragment.chronometer_daily.stop(); //why it resets and starts from the beginning??
    }
}
*/