package com.example.teamproject.ui.Activity;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.Categories.CompetitiveFragment;
import com.example.teamproject.ui.Activity.Categories.OutdoorFragment;
import com.example.teamproject.ui.Threats.ThreatsFragment;

public class ActivityFragment extends Fragment {

    private ActivityViewModel activityViewModel;

    CategoriesFragment categoriesFragment;

    public static ActivityFragment newInstance() { return new ActivityFragment(); }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment, container, false);

        categoriesFragment = new CategoriesFragment();
        setFragment(categoriesFragment);

        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activityViewModel = new ViewModelProvider(this).get(ActivityViewModel.class);
        // TODO: Use the ViewModel
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_categories, fragment);
        fragmentTransaction.commit();
    }
}