package com.example.teamproject.ui.Activity.Categories;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.CategoriesFragment;

public class ArtsFragment extends Fragment {
    CategoriesFragment categoriesFragment;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_arts_fragment, container, false);

        FavouritesActivities.favourites = false;

        Button fav = view.findViewById(R.id.btn_favourites);
        fav.setVisibility(View.INVISIBLE);
        categoriesFragment = new CategoriesFragment();

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                setFragment(categoriesFragment);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        return view;
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_categories, fragment);
        fragmentTransaction.commit();
    }
}
