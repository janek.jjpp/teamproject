package com.example.teamproject.ui.Suggestions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.ActivityDetails;
import com.example.teamproject.ui.Activity.CategoriesFragment;
import com.example.teamproject.ui.Threats.ListFragment;

import java.util.ArrayList;
import java.util.List;

public class FavouritesSuggestions extends Fragment {
    ActivitySuggestions activitySuggestions;
    ListView fav;
    TextView notification;
    List<String> myList;
    public static Boolean favourites = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourites_acivities, container, false);

        fav = (ListView) view.findViewById(R.id.fav_list);
        myList = new ArrayList<String>();
        myList = ActivityDetails.favourites;
        List<Integer> myImages = new ArrayList<>();

        for (String temp : myList) {
            switch (temp){
                case("chess"):
                    myImages.add(R.drawable.chess);
                    break;
                case("card games"):
                    myImages.add(R.drawable.cardgames);
                    break;
                case("puzzles"):
                    myImages.add(R.drawable.puzzles);
                    break;
                case("rubik's cube"):
                    myImages.add(R.drawable.rubik);
                    break;
                case("board games"):
                    myImages.add(R.drawable.board);
                    break;
                case("e-sport and games"):
                    myImages.add(R.drawable.esport);
                    break;
                case("8 ball pool"):
                    myImages.add(R.drawable.balls);
                    break;
                case("arcade games"):
                    myImages.add(R.drawable.arcade);
                    break;
                case("bowling"):
                    myImages.add(R.drawable.bowling);
                    break;
                case("auto racing"):
                    myImages.add(R.drawable.autoracing);
                    break;
                case("yoga"):
                    myImages.add(R.drawable.yoga);
                    break;
                case("cycling"):
                    myImages.add(R.drawable.cycling);
                    break;
                case("swimming"):
                    myImages.add(R.drawable.swimming);
                    break;
                case("running"):
                    myImages.add(R.drawable.running);
                    break;
                case("skiing"):
                    myImages.add(R.drawable.skiing);
                    break;
                case("hiking"):
                    myImages.add(R.drawable.hiking);
                    break;
                case("martial arts"):
                    myImages.add(R.drawable.martial);
                    break;
                case("skydiving"):
                    myImages.add(R.drawable.skydiving);
                    break;
                case("fishing"):
                    myImages.add(R.drawable.fishing);
                    break;
                case("working"):
                    myImages.add(R.drawable.working);
                    break;
                case("guitar"):
                    myImages.add(R.drawable.guitar);
                    break;
                case("singing"):
                    myImages.add(R.drawable.singing);
                    break;
                case("piano"):
                    myImages.add(R.drawable.piano);
                    break;
                case("beatboxing"):
                    myImages.add(R.drawable.beatboxing);
                    break;
                case("rapping"):
                    myImages.add(R.drawable.rapping);
                    break;
                case("drums"):
                    myImages.add(R.drawable.drums);
                    break;
                case("harmonica"):
                    myImages.add(R.drawable.harmonica);
                    break;
                case("bass"):
                    myImages.add(R.drawable.bass);
                    break;
                case("violin"):
                    myImages.add(R.drawable.violin);
                    break;
                case("flute"):
                    myImages.add(R.drawable.flute);
                    break;
                case("reading"):
                    myImages.add(R.drawable.reading);
                    break;
                case("microscopy"):
                    myImages.add(R.drawable.audio);
                    break;
                case("audio"):
                    myImages.add(R.drawable.audio);
                    break;
                case("physics"):
                    myImages.add(R.drawable.physics);
                    break;
                case("languages"):
                    myImages.add(R.drawable.languages);
                    break;
                case("chemistry"):
                    myImages.add(R.drawable.chemistry);
                    break;
                case("astronomy"):
                    myImages.add(R.drawable.astronomy);
                    break;
                case("geography"):
                    myImages.add(R.drawable.geography);
                    break;
                case("history"):
                    myImages.add(R.drawable.history);
                    break;
                case("psychology"):
                    myImages.add(R.drawable.psychology);
                    break;

            }
        }

        if(myList.isEmpty()){
            //show notification
            notification = view.findViewById(R.id.notification);
            notification.setText("Your favourites list is empty!!!");
        }

        MyAdapter adapter = new MyAdapter(getActivity(), myList, myImages);
        fav.setAdapter(adapter);

        activitySuggestions = new ActivitySuggestions();
        fav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String current = myList.get(position);
                seeDetails(current);
            }
        });


        SetSuggestions setSuggestions = new SetSuggestions();

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                setFragment(setSuggestions);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        return view;
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_suggestions, fragment);
        fragmentTransaction.commit();
    }
    class MyAdapter extends ArrayAdapter<String>{

        Context context;
        List<String> tTitles;
        List<Integer> tImages;

        MyAdapter(Context context, List<String> title, List<Integer> images) {
            super(context, R.layout.threats_item, title);
            this.context = context;
            this.tTitles = title;
            this.tImages = images;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View fav = layoutInflater.inflate(R.layout.threats_item, parent, false);

            ImageView images = fav.findViewById(R.id.threat_image);
            TextView title = fav.findViewById(R.id.threat_title);

            images.setImageResource(tImages.get(position));
            title.setText(tTitles.get(position));

            return fav;
        }
    }
    public void seeDetails(String current){
        switch (current) {
            case ("chess"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "chess";
                favourites = true;
                break;
            case ("card games"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "card games";
                favourites = true;
                break;
            case ("puzzles"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "puzzles";
                favourites = true;
                break;
            case ("rubik's cube"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "rubik's cube";
                favourites = true;
                break;
            case ("board games"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "board games";
                favourites = true;
                break;
            case ("e-sport and games"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "e-sport and games";
                favourites = true;
                break;
            case ("8 ball pool"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "8 ball pool";
                favourites = true;
                break;
            case ("arcade games"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "arcade games";
                favourites = true;
                break;
            case ("bowling"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "bowling";
                favourites = true;
                break;
            case ("auto racing"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "auto racing";
                favourites = true;
                break;
            case ("yoga"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "yoga";
                favourites = true;
                break;
            case ("cycling"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "cycling";
                favourites = true;
                break;
            case ("swimming"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "swimming";
                favourites = true;
                break;
            case ("running"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "running";
                favourites = true;
                break;
            case ("skiing"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "skiing";
                favourites = true;
                break;
            case ("hiking"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "hiking";
                favourites = true;
                break;
            case ("martial arts"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "martial arts";
                favourites = true;
                break;
            case ("skydiving"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "skydiving";
                favourites = true;
                break;
            case ("fishing"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "fishing";
                favourites = true;
                break;
            case ("working"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "working";
                favourites = true;
                break;
            case ("guitar"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "guitar";
                favourites = true;
                break;
            case ("singing"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "singing";
                favourites = true;
                break;
            case ("piano"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "piano";
                favourites = true;
                break;
            case ("beatboxing"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "beatboxing";
                favourites = true;
                break;
            case ("rapping"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "rapping";
                favourites = true;
                break;
            case ("drums"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "drums";
                favourites = true;
                break;
            case ("harmonica"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "harmonica";
                favourites = true;
                break;
            case ("bass"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "bass";
                favourites = true;
                break;
            case ("violin"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "violin";
                favourites = true;
                break;
            case ("flute"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "flute";
                favourites = true;
                break;
            case ("reading"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "reading";
                favourites = true;
                break;
            case ("microscopy"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "microscopy";
                favourites = true;
                break;
            case ("audio"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "audio";
                favourites = true;
                break;
            case ("physics"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "physics";
                favourites = true;
                break;
            case ("languages"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "languages";
                favourites = true;
                break;
            case ("chemistry"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "chemistry";
                favourites = true;
                break;
            case ("astronomy"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "astronomy";
                favourites = true;
                break;
            case ("geography"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "geography";
                favourites = true;
                break;
            case ("history"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "history";
                favourites = true;
                break;
            case ("psychology"):
                setFragment(activitySuggestions);
                CategoriesFragment.btn = "psychology";
                favourites = true;
                break;

        }
    }
}