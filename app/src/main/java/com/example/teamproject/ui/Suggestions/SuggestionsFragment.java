package com.example.teamproject.ui.Suggestions;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.Categories.FavouritesActivities;
import com.example.teamproject.ui.Activity.Categories.MusicFragment;
import com.example.teamproject.ui.Threats.ThreatsFragment;
import com.example.teamproject.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Random;

import static java.lang.System.exit;

public class SuggestionsFragment extends Fragment {

    SetSuggestions setSuggestions;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.suggestions_fragment, container, false);

        setSuggestions = new SetSuggestions();
        setFragment(setSuggestions);

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                Toast.makeText(getContext(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                if (doubleBackToExitPressedOnce)
                    exit(1);
                else
                    doubleBackToExitPressedOnce = true;
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        return view;
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_suggestions, fragment);
        fragmentTransaction.commit();
    }
}