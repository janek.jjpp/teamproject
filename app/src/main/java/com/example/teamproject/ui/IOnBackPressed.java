package com.example.teamproject.ui;

public interface IOnBackPressed {
    boolean onBackPressed();
}