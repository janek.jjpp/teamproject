package com.example.teamproject.ui.Threats;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.teamproject.R;

public class ArticlesFragment extends Fragment{

    ThreatsFragment threatsFragment = new ThreatsFragment();
    Article1 article1;
    Article2 article2;
    ListView articles_list;
    String Titles[] = {"Social Media and Social Anxiety Disorder", "How FOMO Impacts Teens and Young Adults", "Article 3", "Article 4"};
    String Authors[] = {"Arlin Cuncic", "Sherri Gordon", "author 3", "author 4"};
    int Images[] = {R.drawable.ar1, R.drawable.ar2, R.drawable.icon_photo_pattern, R.drawable.icon_photo_pattern};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_articles, container, false);
        articles_list = (ListView) view.findViewById(R.id.articles_list);
        MyAdapter adapter = new MyAdapter(getActivity(), Titles, Authors, Images);
        articles_list.setAdapter(adapter);

        article1 = new Article1();
        article2 = new Article2();

        articles_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position){
                    case 0:
                        //Toast.makeText(getActivity(), "content of article 1", Toast.LENGTH_SHORT).show();
                        changeFragment(article1);
                        break;
                    case 1:
                        //Toast.makeText(getActivity(), "content of article 2", Toast.LENGTH_SHORT).show();
                        changeFragment(article2);
                        break;
                    case 2:
                        //Toast.makeText(getActivity(), "content of article 3", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        //Toast.makeText(getActivity(), "content of article 4", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        return view;
    }
    class MyAdapter extends ArrayAdapter<String>{

        Context context;
        String rTitles[];
        String rAuthors[];
        int rImages[];

        MyAdapter(Context context, String title[], String author[], int images[]) {
            super(context, R.layout.articles_item, title);
            this.context = context;
            this.rTitles = title;
            this.rAuthors = author;
            this.rImages = images;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View articles = layoutInflater.inflate(R.layout.articles_item, parent, false);

            ImageView images = articles.findViewById(R.id.article_image);
            TextView title = articles.findViewById(R.id.article_title);
            TextView author = articles.findViewById(R.id.article_author);

            images.setImageResource(rImages[position]);
            title.setText(rTitles[position]);
            author.setText(rAuthors[position]);

            return articles;
        }
    }

    public void changeFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment);
        ThreatsFragment.button_threats.setVisibility(View.INVISIBLE);
        ThreatsFragment.button_articles.setVisibility(View.INVISIBLE);
        fragmentTransaction.commit();
    }
}