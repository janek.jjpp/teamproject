package com.example.teamproject.ui.Threats;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.teamproject.R;

import static java.lang.System.exit;

public class  ThreatsFragment extends Fragment {

    private ThreatsViewModel threatsViewModel;
    ListFragment listFragment;
    ArticlesFragment articlesFragment;

    public static Button button_articles;
    public static Button button_threats;
    public static boolean fromArticles = false;

    private boolean doubleBackToExitPressedOnce = false;

    public static ThreatsFragment newInstance() {
        return new ThreatsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.threats_fragment, container, false);

        listFragment = new ListFragment();
        articlesFragment = new ArticlesFragment();
        button_threats = view.findViewById(R.id.btn_threats);
        button_articles = view.findViewById(R.id.btn_articles);

        check();

        button_articles.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                fromArticles = true;
                check();
            }
        });
        button_threats.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                fromArticles = false;
                check();
            }
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                Toast.makeText(getContext(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                if (doubleBackToExitPressedOnce)
                  exit(1);
                else
                    doubleBackToExitPressedOnce = true;
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        threatsViewModel = new ViewModelProvider(this).get(ThreatsViewModel.class);
    }

    public void check(){
        if(!fromArticles) {
            setFragment(listFragment);
            button_articles.setForeground(null);
            button_threats.setForeground(getResources().getDrawable(R.drawable.button_border));
        }
        else{
            setFragment(articlesFragment);
            button_threats.setForeground(null);
            button_articles.setForeground(getResources().getDrawable(R.drawable.button_border));
        }
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment);
        fragmentTransaction.commit();
    }
}