package com.example.teamproject.ui.Activity;

import androidx.fragment.app.FragmentTransaction;
import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.Categories.ArtsFragment;
import com.example.teamproject.ui.Activity.Categories.CollectingFragment;
import com.example.teamproject.ui.Activity.Categories.CompetitiveFragment;
import com.example.teamproject.ui.Activity.Categories.CraftsFragment;
import com.example.teamproject.ui.Activity.Categories.EducationFragment;
import com.example.teamproject.ui.Activity.Categories.ElectronicsFragment;
import com.example.teamproject.ui.Activity.Categories.FavouritesActivities;
import com.example.teamproject.ui.Activity.Categories.MusicFragment;
import com.example.teamproject.ui.Activity.Categories.OthersFragment;
import com.example.teamproject.ui.Activity.Categories.OutdoorFragment;
import com.example.teamproject.ui.Activity.Categories.SpiritualFragment;

public class CategoriesFragment extends Fragment {

    public static String btn; //what the user clicked
    Button favourites_activities;
    FavouritesActivities favouritesActivities;

    CompetitiveFragment competitiveFragment;
    OutdoorFragment outdoorFragment;
    MusicFragment musicFragment;
    EducationFragment educationFragment;
    CollectingFragment collectingFragment;
    ArtsFragment artsFragment;
    ElectronicsFragment electronicsFragment;
    CraftsFragment craftsFragment;
    SpiritualFragment spiritualFragment;
    OthersFragment othersFragment;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.categories_fragment, container, false);

        favouritesActivities = new FavouritesActivities();
        favourites_activities = view.findViewById(R.id.btn_favourites);
        favourites_activities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { setFragment(favouritesActivities); }
        });

        Button competitive = view.findViewById(R.id.btn_competitive);
        Button outdoor = view.findViewById(R.id.btn_outdoor);
        Button music = view.findViewById(R.id.btn_music);
        Button education = view.findViewById(R.id.btn_education);
        Button collecting = view.findViewById(R.id.btn_collecting);
        Button arts = view.findViewById(R.id.btn_arts);
        Button electronics = view.findViewById(R.id.btn_electronics);
        Button crafts = view.findViewById(R.id.btn_crafts);
        Button spiritual = view.findViewById(R.id.btn_spiritual);
        Button others = view.findViewById(R.id.btn_others);

        competitiveFragment = new CompetitiveFragment();
        outdoorFragment = new OutdoorFragment();
        musicFragment = new MusicFragment();
        educationFragment = new EducationFragment();
        collectingFragment = new CollectingFragment();
        artsFragment = new ArtsFragment();
        electronicsFragment = new ElectronicsFragment();
        craftsFragment = new CraftsFragment();
        spiritualFragment = new SpiritualFragment();
        othersFragment = new OthersFragment();

        competitive.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) { setFragment(competitiveFragment); }
        });
        outdoor.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(outdoorFragment);
            }
        });
        music.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(musicFragment);
            }
        });
        education.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(educationFragment);
            }
        });
        collecting.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(collectingFragment);
            }
        });
        arts.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(artsFragment);
            }
        });
        electronics.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(electronicsFragment);
            }
        });
        crafts.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(craftsFragment);
            }
        });
        spiritual.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(spiritualFragment);
            }
        });
        others.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(othersFragment);
            }
        });
        return view;
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_categories, fragment);
        fragmentTransaction.commit();
    }
}