package com.example.teamproject.ui.Activity.Categories;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.style.TtsSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.ActivityDetails;
import com.example.teamproject.ui.Activity.CategoriesFragment;

public class EducationFragment extends Fragment {

    ActivityDetails activityDetails;
    CategoriesFragment categoriesFragment;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_education_fragment, container, false);

        FavouritesActivities.favourites = false;

        Button competitive = view.findViewById(R.id.btn_competitive);
        Button outdoor = view.findViewById(R.id.btn_outdoor);
        Button music = view.findViewById(R.id.btn_music);
        Button education = view.findViewById(R.id.btn_education);
        Button collecting = view.findViewById(R.id.btn_collecting);
        Button arts = view.findViewById(R.id.btn_arts);
        Button electronics = view.findViewById(R.id.btn_electronics);
        Button crafts = view.findViewById(R.id.btn_crafts);
        Button spiritual = view.findViewById(R.id.btn_spiritual);
        Button others = view.findViewById(R.id.btn_others);

        Button fav = view.findViewById(R.id.btn_favourites);
        fav.setVisibility(View.INVISIBLE);

        activityDetails = new ActivityDetails();
        categoriesFragment = new CategoriesFragment();

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                setFragment(categoriesFragment);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        competitive.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "reading";
            }
        });
        outdoor.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "microscopy";
            }
        });
        music.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "audio";
            }
        });
        education.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "physics";
            }
        });
        collecting.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "languages";
            }
        });
        arts.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "chemistry";
            }
        });
        electronics.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "astronomy";
            }
        });
        crafts.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "geography";
            }
        });
        spiritual.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "history";
            }
        });
        others.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                setFragment(activityDetails);
                CategoriesFragment.btn = "psychology";
            }
        });

        return view;
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_categories, fragment);
        fragmentTransaction.commit();
    }
}
