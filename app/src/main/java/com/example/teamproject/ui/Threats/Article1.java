package com.example.teamproject.ui.Threats;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.teamproject.R;
import com.github.barteksc.pdfviewer.PDFView;

public class Article1 extends Fragment {

    ArticlesFragment articlesFragment = new ArticlesFragment();
    PDFView article1;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.article1_fragment, container, false);

        article1 = view.findViewById(R.id.article1pdf);
        article1.fromAsset("article1.pdf").load();

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                setFragment(articlesFragment);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        return view;
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        article1.setVisibility(View.INVISIBLE);
        ThreatsFragment.button_articles.setVisibility(View.VISIBLE);
        ThreatsFragment.button_threats.setVisibility(View.VISIBLE);
        fragmentTransaction.replace(R.id.fr1, fragment);
        fragmentTransaction.commit();
    }
}
