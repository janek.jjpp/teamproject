package com.example.teamproject.ui.home;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.example.teamproject.MainActivity;
import com.example.teamproject.R;
import com.example.teamproject.ui.Data.DataFragment;
import com.example.teamproject.ui.Threats.ThreatsFragment;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Random;

import static java.lang.System.exit;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    int appsAmount = 12;
    //public static Chronometer chronometer_daily;
    TextView chronometer_daily;
    TextView chronometer_weekly;
    TextView chronometer_monthly;
    public static long lastValue;
    String day;
    TextView daily;
    TextView monthly;
    TextView weekly;
    ProgressBar daily_bar;
    ProgressBar monthly_bar;
    ProgressBar weekly_bar;
    public static int average_weekly_minutes;
    public static int average_monthly_minutes;

    private boolean doubleBackToExitPressedOnce = false;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        //homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        View root = inflater.inflate(R.layout.home_fragment, container, false);

        day = new SimpleDateFormat("EEEE, dd-MM-yyyy", Locale.ENGLISH).format(System.currentTimeMillis());

        daily = root.findViewById(R.id.value_daily);
        daily.setText(day);
        monthly = root.findViewById(R.id.value_monthly);
        monthly.setText("this month");
        weekly = root.findViewById(R.id.value_weekly);
        weekly.setText("this week");
        daily_bar = root.findViewById(R.id.progress_bar_daily);
        monthly_bar = root.findViewById(R.id.progress_bar_monthly);
        weekly_bar = root.findViewById(R.id.progress_bar_weekly);

        //chronometer_daily = (Chronometer) root.findViewById(R.id.timer_daily);
        chronometer_daily = root.findViewById(R.id.timer_daily);
        chronometer_weekly = root.findViewById(R.id.timer_weekly);
        chronometer_monthly = root.findViewById(R.id.timer_monthly);

        int average_daily_minutes = ((DataFragment.generalHours * 60) + DataFragment.generalMinutes ) / appsAmount / 2;
        average_weekly_minutes = 9 * average_daily_minutes;
        average_monthly_minutes = 30 * average_daily_minutes;

        chronometer_daily.setText((average_daily_minutes / 60% 12) + "h " + (average_daily_minutes % 60) + "min");
        chronometer_weekly.setText((average_weekly_minutes / 60 % 80) + "h " + (average_weekly_minutes % 60) + "min");
        chronometer_monthly.setText((average_monthly_minutes / 60 % 200) + "h " + (average_monthly_minutes % 60) + "min");

        double progress_daily = ((average_daily_minutes / 60) * 100) / 10 % 12; //24- amount of hours in day
        //System.out.println("day"+progress_daily);
        double progress_weekly = ((average_weekly_minutes / 60) * 100) / 168 * 2  % 80; //168- amount of hours in week
        //System.out.println("week"+progress_weekly);
        double progress_monthly = ((average_monthly_minutes / 60) * 100) / 720 * 5 % 200; //720- amount of hours in month
        //System.out.println("month"+progress_monthly);

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                Toast.makeText(getContext(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                if (doubleBackToExitPressedOnce)
                    exit(1);
                else
                    doubleBackToExitPressedOnce = true;
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        daily_bar.setProgress((int)Math.round(progress_daily)); //rounded daily_value/24h
        weekly_bar.setProgress((int)Math.round(progress_weekly)); //rounded weekly_value/168h
        monthly_bar.setProgress((int)Math.round(progress_monthly)); //rounded monthly_value/(30 or 31)
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        day = new SimpleDateFormat("EEEE, dd-MM-yyyy", Locale.ENGLISH).format(System.currentTimeMillis());
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
    }
}