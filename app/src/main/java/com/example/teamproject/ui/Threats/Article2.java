package com.example.teamproject.ui.Threats;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.teamproject.R;
import com.github.barteksc.pdfviewer.PDFView;

public class Article2 extends Fragment {

    ArticlesFragment articlesFragment = new ArticlesFragment();
    PDFView article2;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.article2_fragment, container, false);

        article2 = view.findViewById(R.id.article2pdf);
        article2.fromAsset("article2.pdf").load();

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                setFragment(articlesFragment);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        return view;
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        article2.setVisibility(View.INVISIBLE);
        ThreatsFragment.button_articles.setVisibility(View.VISIBLE);
        ThreatsFragment.button_threats.setVisibility(View.VISIBLE);
        fragmentTransaction.replace(R.id.fr2, fragment);
        fragmentTransaction.commit();
    }
}
