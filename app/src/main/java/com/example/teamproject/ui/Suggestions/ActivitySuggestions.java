package com.example.teamproject.ui.Suggestions;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.ActivityDetails;
import com.example.teamproject.ui.Activity.Categories.ArtsFragment;
import com.example.teamproject.ui.Activity.Categories.CollectingFragment;
import com.example.teamproject.ui.Activity.Categories.CompetitiveFragment;
import com.example.teamproject.ui.Activity.Categories.CraftsFragment;
import com.example.teamproject.ui.Activity.Categories.EducationFragment;
import com.example.teamproject.ui.Activity.Categories.ElectronicsFragment;
import com.example.teamproject.ui.Activity.Categories.FavouritesActivities;
import com.example.teamproject.ui.Activity.Categories.MusicFragment;
import com.example.teamproject.ui.Activity.Categories.OthersFragment;
import com.example.teamproject.ui.Activity.Categories.OutdoorFragment;
import com.example.teamproject.ui.Activity.Categories.SpiritualFragment;
import com.example.teamproject.ui.Activity.CategoriesFragment;
import com.example.teamproject.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class ActivitySuggestions extends Fragment {

    CompetitiveFragment competitiveFragment;
    OutdoorFragment outdoorFragment;
    MusicFragment musicFragment;
    EducationFragment educationFragment;
    CollectingFragment collectingFragment;
    ArtsFragment artsFragment;
    ElectronicsFragment electronicsFragment;
    CraftsFragment craftsFragment;
    SpiritualFragment spiritualFragment;
    OthersFragment othersFragment;

    public ImageView imgView;
    public TextView description;
    Button btn_fav;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity_details, container, false);

        competitiveFragment = new CompetitiveFragment();
        outdoorFragment = new OutdoorFragment();
        musicFragment = new MusicFragment();
        educationFragment = new EducationFragment();
        collectingFragment = new CollectingFragment();
        artsFragment = new ArtsFragment();
        electronicsFragment = new ElectronicsFragment();
        craftsFragment = new CraftsFragment();
        spiritualFragment = new SpiritualFragment();
        othersFragment = new OthersFragment();

        imgView = (ImageView) view.findViewById(R.id.photo);
        description = view.findViewById(R.id.description);

        btn_fav = view.findViewById(R.id.fav);
        String current = CategoriesFragment.btn;

        if (ActivityDetails.favourites.contains(current)) {
            btn_fav.setText("Delete from favourites");
        } else {
            btn_fav.setText("Add to favourites");
        }
        btn_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn_fav.getText().equals("Add to favourites")) {
                    ActivityDetails.favourites.add(current);
                    btn_fav.setText("Delete from favourites");
                } else {
                    ActivityDetails.favourites.remove(current);
                    btn_fav.setText("Add to favourites");
                }
            }
        });

        FavouritesSuggestions favouritesSuggestions = new FavouritesSuggestions();
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                setFragment(favouritesSuggestions);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        openDetails(CategoriesFragment.btn);
        return view;
    }
    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_suggestions, fragment);
        fragmentTransaction.commit();
    }
    public void openDetails(String current){
        switch (current) {
            case ("chess"):
                imgView.setImageResource(R.drawable.chess);
                description.setText("Chess is a board game for two players. It is played in a square board, made of 64 smaller squares, with eight squares on each side. Each player starts with sixteen pieces: eight pawns, two knights, two bishops, two rooks, one queen and one king. The goal of the game is for each player to try and checkmate the king of the opponent. Checkmate is a threat ('check') to the opposing king which no move can stop. It ends the game.\n" +
                        "During the game the two opponents take turns to move one of their pieces to a different square of the board. One player ('White') has pieces of a light color; the other player ('Black') has pieces of a dark color. There are rules about how pieces move, and about taking the opponent's pieces off the board. The player with white pieces always makes the first move. Because of this, White has a small advantage, and wins more often than Black in tournament games.\n" +
                        "Chess is popular and is often played in competitions called chess tournaments. It is enjoyed in many countries, and is a national hobby in Russia.");
                break;
            case ("card games"):
                imgView.setImageResource(R.drawable.cardgames);
                description.setText("A card game is any game played with playing cards. Some common card games are poker, bridge, blackjack, solitaire, and go fish.\n" +
                        "Cards are individually identifiable from one side only (the \"face\"). Each player knows only the cards he holds and not those held by anyone else. Card games are often called \"games of chance\" or games of \"imperfect information\". In these, the current position is not seen by all players throughout the game.\n" +
                        "Some games of cards use a board. The gameplay of a card game depends on the use of the cards by players. The board is a guide for scorekeeping or for card placement. Board games (the principal non-card game genre to use cards) generally focus on the players' positions on the board, and use the cards for some secondary purpose.\n" +
                        "There are trading card games like Pokemon and Yu-Gi-Oh! that use special packs. These games often involve fighting role-play.");
                break;
            case ("puzzles"):
                imgView.setImageResource(R.drawable.puzzles);
                description.setText("A puzzle is a game, problem, or toy that tests a person's ingenuity or knowledge. In a puzzle, the solver is expected to put pieces together in a logical way, in order to arrive at the correct or fun solution of the puzzle. There are different genres of puzzles, such as crossword puzzles, word-search puzzles, number puzzles, relational puzzles, and logic puzzles.\n" +
                        "Puzzles are often created to be a form of entertainment but they can also arise from serious mathematical or logical problems. In such cases, their solution may be a significant contribution to mathematical research.");
                break;
            case ("rubik's cube"):
                imgView.setImageResource(R.drawable.rubik);
                description.setText("Rubik's Cube is a puzzle cube, and the world's biggest selling toy of all time with over 300,000,000 (300 million) sold. It was invented in 1974 by Hungarian sculptor and professor of architecture Ernő Rubik. In a classic Rubik's Cube, each of the six faces is covered by nine stickers, each of one of six solid colours: white, red, blue, orange, green, and yellow.\n" +
                        "The cubes can be rotated and twisted along rows and faces. The aim of the puzzle is to make each face of the cube have the same color. Solving a Rubik's Cube requires logic and thinking. There are many different ways to learn how to solve a Rubik's cube.");
                break;
            case ("board games"):
                imgView.setImageResource(R.drawable.board);
                description.setText("A board game is a game usually played with pieces on a board, or some area with marked spaces.\n" +
                        "Most board games use pieces that may be moved, placed, or traded depending on the rules of the game. These pieces may be money, chips, pawns, or other objects. Board games may often involve some random chance with dice or cards. There are many board games with a long history in some cultures. Examples of these games are chess, checkers, backgammon, parqués, and go. There are also a great number of popular board games that have been created more recently, in the past hundred years. Among these games are Scrabble and Monopoly.");
                break;
            case ("e-sport and games"):
                imgView.setImageResource(R.drawable.esport);
                description.setText("Video games are electronic games played on a video screen (normally a television, a built-in screen when played on a handheld machine, or a computer).\n" +
                        "There are many types, or genres, of these games: role-playing games; shooters, first-person shooters, side-scrollers, and platformers are just a few.\n" +
                        "People can play portable video games anywhere. Mobile also can download games, making them portable game machines. Mobile phones have many games, some of them using a mobile emulator for games from consoles. \n" +
                        "Competitions of video game players are called electronic sports.\n" +
                        "Electronic sports (Also called esports) is term for video games that are played competitively. Popular electronic sports games are Counter-Strike, League of Legends, Warcraft, Starcraft, Dota 2, and Quake. Electronic Sports is played over the Internet or via LAN.");
                break;
            case ("8 ball pool"):
                imgView.setImageResource(R.drawable.balls);
                description.setText("Eight-ball (also spelled 8-ball) is a pool billiards played on a billiard table with six pockets, cue sticks, and sixteen billiard balls: a cue ball and fifteen object balls. The object balls include seven solid-colored balls numbered 1 through 7, seven striped balls numbered 9 through 15, and the black 8 ball. After the balls are scattered with a break shot, a player is assigned either the group of solid or striped balls once they have legally pocketed a ball from that group. The object of the game is to legally pocket the 8 ball in a \"called\" pocket, which can only be done after all of the balls from a player's assigned group have been cleared from the table.");
                break;
            case ("arcade games"):
                imgView.setImageResource(R.drawable.arcade);
                description.setText("An arcade game or coin-op game is a coin-operated entertainment machine typically installed in public businesses such as restaurants, bars and amusement arcades. Most arcade games are presented as primarily games of skill and include arcade video games, Pinball machines, electro-mechanical games, redemption games or merchandisers.");
                break;
            case ("bowling"):
                imgView.setImageResource(R.drawable.bowling);
                description.setText("Bowling is a sport where the bowler rolls the ball down a wooden bowling lane trying to knock down bowling pins. This kind includes ten-pin bowling, which is so commonplace that it is often simply called \"bowling\". Some people bowl at smaller pins, called \"duck pins\" or \"candle pins\" with slightly different rules. The usual kind of bowling ball has three holes, where the player puts two fingers and the thumb. The person on the lane swings the ball and lets it go to roll along the lane, attempting to knock down ten wooden pins. If the bowler does not do it right, the ball might not knock all the pins down or might roll into the gutter and hit none. Players take turns rolling their ball down the lane to see who gets the highest score.");
                break;
            case ("auto racing"):
                imgView.setImageResource(R.drawable.autoracing);
                description.setText("Auto racing (also known as car racing) is a motorsport involving the racing of automobiles for competition. There are numerous different categories, each with different rules and regulations.\n" +
                        "The two most popular varieties of open-wheel road racing are Formula One and the IndyCar Series. F1 is a worldwide series that runs only street circuit and race tracks. These cars are heavily based around technology and their aerodynamics.\n" +
                        "Rally is a form of motorsport that takes place on public or private roads with modified production or specially built road-legal cars. It is distinguished by not running on a circuit, but instead in a point-to-point format in which participants and their co-drivers drive between set control points (special stages), leaving at regular intervals from one or more start points.\n" +
                        "Auto racing began in France in 1895 and is now one of the world's most popular spectator sports.");
                break;
            case ("yoga"):
                imgView.setImageResource(R.drawable.yoga);
                description.setText("A person doing yoga will move from one posture (called asana) to another, but there are also other aspects which are important in yoga, such as breath-work, mantras, mudras (postures of the hands and fingers), meditation and much more. For example, the \"sun-salutation\" contains 12 poses of asanas, one after the other, and is said to help balance body and soul. There is a specific mantra for each asana. \n" +
                        "Now yoga has been added to mainstream workouts for stress relief, increased flexibility, and strength training. Highly successful commercial fitness programs often include “power” yoga for fat-burning and weight loss.");
                break;
            case ("cycling"):
                imgView.setImageResource(R.drawable.cycling);
                description.setText("Riding bicycles, which is also called cycling, is an important way to travel in several parts of the world. The most popular type of cycling is Utility cycling. It is also a common recreation, a good form of low-impact exercise, and a popular sport. Road bicycle racing is the second most popular spectator sport in the world. Bicycling uses less energy per mile than any other human transport.\n" +
                        "Cycle sport is competitive physical activity using bicycles. There are several categories of bicycle racing including road bicycle racing, time trialling, cyclo-cross, mountain bike racing, track cycling, BMX, and cycle speedway. Non-racing cycling sports include artistic cycling, cycle polo, freestyle BMX and mountain bike trials.");
                break;
            case ("swimming"):
                imgView.setImageResource(R.drawable.swimming);
                description.setText("Swimming is the movement of the body through water using arms and legs. People can swim in the sea, swimming pools, rivers and lakes. People swim for exercise, for fun, and as a sport. There are several styles of swimming, known as \"strokes\", including: front crawl, breaststroke, freestyle, butterfly, and backstroke.\n" +
                        "Swimming works all the muscles simultaneously. It is impact free. It also builds up stamina.\n" +
                        "Because it is a low impact exercise, the strain on joints, muscles and your back are very much reduced, and it is also good for your circulation, blood pressure and cholesterol levels. Swimming tones the body, strengthens the muscles, improves stamina and balance, relieves stress and tension, and is also a great way to burn fat.");
                break;
            case ("running"):
                imgView.setImageResource(R.drawable.running);
                description.setText("Running is the way in which people or animals travel quickly on their feet. It is a method of travelling on land. It is different to walking in that both feet are regularly off the ground at the same time. Different terms are used to refer to running according to the speed: jogging is slow, and sprinting is running fast.\n" +
                        "Running is a popular form of exercise. It is also one of the oldest forms of sport. The exercise is known to be good for health; it helps breathing and heartbeat, and burns any spare calories. Running keeps a person fit and active. It also relieves stress. Running makes a person thirsty, so it is important to drink water when running.\n" +
                        "Running is a part of many forms of competitive racing. Most running races test speed, endurance or both. Track and field races are usually divided into sprints, middle-distance races and long-distance races. Races held off the track may be called cross-country races. A marathon is run over 42 kilometres.\n" +
                        "Footraces have probably existed for most of human history. They were an important part of the ancient Olympic Games.");
                break;
            case ("skiing"):
                imgView.setImageResource(R.drawable.skiing);
                description.setText("Skiing is either sportive or recreational activity using skis for sliding over snow. Skis are used with special boots connected to them with a binding. There are two different types of skiing: downhill skiing and cross country skiing.\n" +
                        "Downhill skiing is a sport. Skiers ski down a trail (also called a \"run\") on the side of a mountain or hill. Each trail is marked with a sign that shows how difficult that particular run will be. Three different colors are used to rate the difficulty of the trail. They are: green, blue, and black. Green trails are easiest, blue trails are more difficult, and black trails are rated most difficult.\n" +
                        "The Nordic disciplines include cross-country skiing and ski jumping, which both use bindings that attach at the toes of the skier's boots but not at the heels. Cross-country skiing may be practiced on groomed trails or in undeveloped backcountry areas. Ski jumping is practiced in certain areas that are reserved exclusively for ski jumping.");
                break;
            case ("hiking"):
                imgView.setImageResource(R.drawable.hiking);
                description.setText("Hiking is a general word for traveling on foot in wilderness or countryside. Day hikers return before nightfall. Other hikers may go out for many days. Sometimes large groups hike together.\n" +
                        "Hiking is a kind of physical activity as well as a leisure activity (a way of spending free time). Hikers (the people who do hiking) enjoy fresh air and the beauty of nature, and learn about the place. Many hikers enjoy challenging their physical and mental strength. Hiking with friends gives them the chance to become stronger.\n" +
                        "Hiking in winter offers additional opportunities, challenges and hazards. Crampons may be needed in icy conditions, and an ice ax is recommended on steep, snow covered paths. Snowshoes and hiking poles, or cross country skis are useful aid for those hiking in deep snow.");
                break;
            case ("martial arts"):
                imgView.setImageResource(R.drawable.martial);
                description.setText("A martial art is any form of fighting and an art that has a set way of practice. There are many martial arts that come from certain countries. They are practiced for many reasons: fighting, self-defense, sport, self-expression, discipline, confidence, fitness, relaxing, meditation. A martial art is a style of combat, in many instances directed towards the self-defence. In the common usage, the word applies to the systems of combat developed in all the world.\n" +
                        "Many martial arts include form of punches (boxing, karate), kicks (taekwondo, kickboxing, karate), holds and throws (judo, jujutsu, wrestling), weapons (iaijutsu, kendo, kenjutsu, naginatado, fencing, Filipino eskrima) or certain combination of these elements (several styles of jujutsu).\n" +
                        "Martial arts are divided in two main sets: the so-called \"hard martial arts\" like karate and kickboxing which give special consideration to the attack to beat the opponent, and the \"soft martial arts\" like judo and aikido which fight the opponent in a less aggressive manner, using the force of the other to surrender him.");
                break;
            case ("skydiving"):
                imgView.setImageResource(R.drawable.skydiving);
                description.setText("Skydiving is parachuting from an airplane for fun. Skydiving can be done individually and with groups of people. Unlike most paratroopers, skydivers often wait until they are low, before opening the parachute. The jump can also be made from a helicopter or a balloon that is high enough in the sky. \n" +
                        "Skydiving includes free falling (usually from an aeroplane) through the air prior to opening a parachute. Typically skydives are carried out from around 4,000m (or 12,500ft) offering 40 to 50 seconds of freefall time. Longer free fall times can be achieved by exiting an aircraft at altitudes much higher than 4,000m, but very high jumps require pressurized oxygen within the aircraft, and bottled oxygen for the diver.\n" +
                        "During a skydive, total freedom and control of the air can be enjoyed as well as many complex and spectacular manoeuvres including flat turns, somersaults and formation skydiving. Skydiving can be enjoyed either as an individual - doing solo(alone) jumps - or as part of a team carrying out formation skydiving. Generally, the term ‘skydive’ refers to the time spent in freefall from exiting an aircraft to deploying a parachute but skydiving does include some disciplines such as accuracy landings and canopy formation flying which concentrate on the time spent once a canopy has been deployed.");
                break;
            case ("fishing"):
                imgView.setImageResource(R.drawable.fishing);
                description.setText("Fishing is the activity of trying to catch fish. Fish are often caught in the wild but may also be caught from stocked bodies of water. Techniques for catching fish include hand gathering, spearing, netting, angling and trapping. \"Fishing\" may include catching aquatic animals other than fish, such as molluscs, cephalopods, crustaceans, and echinoderms. \n" +
                        "In addition to being caught to be eaten, fish are caught as recreational pastimes. Fishing tournaments are held, and caught fish are sometimes kept as preserved or living trophies.\n" +
                        "Modern fishing rods have a reel for line stowage and line guides. Commonly the modern fishing rods are made up of carbon fibre, fibre glass or bamboo. There are many different types of fishing rods which vary in size and length as well as the way they are used.");
                break;
            case ("working"):
                imgView.setImageResource(R.drawable.working);
                description.setText("Exercise is any bodily activity that enhances or maintains physical fitness and overall health and wellness.\n" +
                        "It is performed for various reasons, to aid growth and improve strength, preventing aging, developing muscles and the cardiovascular system, honing athletic skills, weight loss or maintenance, improving health and also for enjoyment. Many individuals choose to exercise outdoors where they can congregate in groups, socialize, and enhance well-being.\n" +
                        "In terms of health benefits, the amount of recommended exercise depends upon the goal, the type of exercise, and the age of the person. Even doing a small amount of exercise is healthier than doing none.\n" +
                        "Anaerobic exercise, which includes strength and resistance training, can firm, strengthen, and increase muscle mass, as well as improve bone density, balance, and coordination. Examples of strength exercises are push-ups, pull-ups, lunges, squats, bench press. Anaerobic exercise also includes weight training, functional training, eccentric training, interval training, sprinting, and high-intensity interval training which increase short-term muscle strength.\n" +
                        "Flexibility exercises stretch and lengthen muscles. Activities such as stretching help to improve joint flexibility and keep muscles limber. The goal is to improve the range of motion which can reduce the chance of injury.");
                break;
            case ("guitar"):
                imgView.setImageResource(R.drawable.guitar);
                description.setText("The guitar is a string instrument which is played by plucking the strings. The main parts of a guitar are the body, the fretboard, the headstock and the strings. Guitars are usually made from wood or plastic. Their strings are made of steel or nylon.\n" +
                        "The guitar strings are plucked with the fingers and fingernails of the right hand (or left hand, for left handed players), or a small pick made of thin plastic. This type of pick is called a \"plectrum\" or guitar pick. The left hand holds the neck of the guitar while the fingers pluck the strings. Different finger positions on the fretboard make different notes.\n" +
                        "Guitar-like plucked string instruments have been used for many years. In many countries and at many different time periods, guitars and other plucked string instruments have been very popular, because they are light to carry from place to place, they are easier to learn to play than many other instruments. Guitars are used for many types of music, from Classical to Rock. Most pieces of popular music that have been written since the 1950s are written with guitars.\n");
                break;
            case ("singing"):
                imgView.setImageResource(R.drawable.singing);
                description.setText("Singing is the act of producing musical sounds with the voice. A person who sings is called a singer or vocalist (in jazz and popular music). Singers perform music (arias, recitatives, songs, etc.) that can be sung with or without accompaniment by musical instruments. Singing is often done in an ensemble of musicians, such as a choir of singers or a band of instrumentalists. Singers may perform as soloists or accompanied by anything from a single instrument (as in art song or some jazz styles) up to a symphony orchestra or big band.\n" +
                        "Singing can be formal or informal, arranged, or improvised. It may be done as a form of religious devotion, as a hobby, as a source of pleasure, comfort, or ritual as part of music education or as a profession. Excellence in singing requires time, dedication, instruction, and regular practice. If practice is done regularly then the sounds can become clearer and stronger. Professional singers usually build their careers around one specific musical genre, such as classical or rock, although there are singers with crossover success (singing in more than one genre).\n");
                break;
            case ("piano"):
                imgView.setImageResource(R.drawable.piano);
                description.setText("A piano (also called a pianoforte) is a musical instrument classified as a percussion instrument that is played by pressing keys on a keyboard.\n" +
                        "Bandleaders and choir conductors often learn the piano, as it is an excellent instrument for learning new pieces and songs to lead in performance. Many conductors are trained in piano, because it allows them to play parts of the symphonies they are conducting (using a piano reduction or doing a reduction from the full score), so that they can develop their interpretation. The piano is an essential tool in music education in elementary and secondary schools, and universities and colleges. Most music classrooms and many practice rooms have a piano. Pianos are used to help teach music theory, music history and music appreciation classes, and even non-pianist music professors or instructors may have a piano in their office.\n");
                break;
            case ("beatboxing"):
                imgView.setImageResource(R.drawable.beatboxing);
                description.setText("Beatboxing (also beat boxing or boxing) is a form of vocal percussion primarily involving the art of mimicking drum machines using one's mouth, lips, tongue, and voice. It may also involve vocal imitation of turntablism, and other musical instruments. Beatboxing today is connected with hip-hop culture, often referred to as \"the fifth element\" of hip-hop, although it is not limited to hip-hop music. The term \"beatboxing\" is sometimes used to refer to vocal percussion in general.\n" +
                        "As you train to beatbox - your vocal skills will develop. Beatboxing is also referred to as multi-vocalism and vocal percussion. So, the range of vocal skills in beatboxing will cover imitating sounds, singing and - everything vocal. Now human beatboxing is usually related to the urban and hip-hop genres of music, while vocal percussion relates to a-cappella and rockapella music groups.\n");
                break;
            case ("rapping"):
                imgView.setImageResource(R.drawable.rapping);
                description.setText("Rapping (also MCing) is a musical form of vocal delivery that incorporates \"rhyme, rhythmic speech, and street vernacular\", which is performed or chanted in a variety of ways, usually over a backing beat or musical accompaniment. The components of rap include \"content\" (what is being said), \"flow\" (rhythm, rhyme), and \"delivery\" (cadence, tone). Rap differs from spoken-word poetry in that it is usually performed off time to musical accompaniment. Rap being a primary ingredient of hip hop music, it is commonly associated with that genre in particular; however, the origins of rap predate hip-hop culture by many years.\n" +
                        "Rap is usually delivered over a beat, typically provided by a DJ, turntablist, beatboxer, or performed a cappella without accompaniment. Stylistically, rap occupies a gray area between speech, prose, poetry, and singing. The word, which predates the musical form, originally meant \"to lightly strike\", and is now used to describe quick speech or repartee. Today, the term rap is so closely associated with hip-hop music that many writers use the terms interchangeably.\n");
                break;
            case ("drums"):
                imgView.setImageResource(R.drawable.drums);
                description.setText("A drum kit (or drum set) is a collection of drums, cymbals, and other percussion instruments that is used by a drummer in a musical group. Most contemporary western bands that play rock, pop, jazz, or R&B music include a drummer for purposes including timekeeping and embellishing the musical timbre. The drummer's equipment includes a drum kit which includes various drums, cymbals and an assortment of accessory hardware such as pedals, standing support mechanisms, and drum sticks.\n" +
                        "As well as the primary rhythmic function, in some musical styles, such as world, jazz, classical, and electronica, the drummer is called upon to provide solo and lead performances, at times when the main feature of the music is the rhythmic development.\n" +
                        "There are many tools that a drummer can use for either timekeeping or soloing. These include cymbals (china, crash, ride, splash, hi-hats, etc.), snare, toms, auxiliary percussion (bells, Latin drums, cowbells, temple blocks) and many others. Also there are single, double, and triple bass pedals for the bass drum.\n");
                break;
            case ("harmonica"):
                imgView.setImageResource(R.drawable.harmonica);
                description.setText("The harmonica, also known as a French harp or mouth organ, is a free reed wind instrument used worldwide in many musical genres, notably in blues, American folk music, classical music, jazz, country, and rock. The many types of harmonica include diatonic, chromatic, tremolo, octave, orchestral, and bass versions. A harmonica is played by using the mouth (lips and tongue) to direct air into or out of one (or more) holes along a mouthpiece. Behind each hole is a chamber containing at least one reed. A harmonica reed is a flat, elongated spring typically made of brass, stainless steel, or bronze, which is secured at one end over a slot that serves as an airway. When the free end is made to vibrate by the player's air, it alternately blocks and unblocks the airway to produce sound.");
                break;
            case ("bass"):
                imgView.setImageResource(R.drawable.bass);
                description.setText("The bass guitar, electric bass or simply bass, is the lowest-pitched member of the guitar family. It is a plucked string instrument similar in appearance and construction to an electric or an acoustic guitar, but with a longer neck and scale length, and typically four to six strings or courses. Since the mid-1950s, the bass guitar has largely replaced the double bass in popular music.\n" +
                        "The four-string bass is usually tuned the same as the double bass, which corresponds to pitches one octave lower than the four lowest-pitched strings of a guitar (E, A, D, and G). It is played primarily with the fingers or thumb, or with a pick. In order to be heard at normal performance volumes, electric basses require external amplification.\n" +
                        "Sounds are produced from the strings in a number of ways. The most common form of playing is called fingerstyle, in which the player plucks the fingers upwards with the ends of the fingers. It is common to use the index and middle fingers as the plucking fingers, but players may use more fingers or even employ their thumb to pluck downwards. Other finger-based techniques include slap-and-pop, in which the player strikes the lower-pitched strings firmly with their thumb, and pulls higher-pitched strings upwards and lets them snap against the fretboard, and tapping, in which the player hammers downward with their fingertips on the fretboard.\n");
                break;
            case ("violin"):
                imgView.setImageResource(R.drawable.violin);
                description.setText("The violin is a string instrument which has four strings and is played with a bow. The strings are usually tuned to the notes G, D, A, and E. It is held between the left collar bone (near the shoulder) and the chin. Different notes are made by fingering (pressing on the strings) with the left hand while bowing with the right. Unlike guitar, it has no frets or other markers on the fingerboard. The violin is the smallest and highest pitched string instrument typically used in western music.\n" +
                        "Violins are important instruments in a wide variety of musical genres. They are most prominent in the Western classical tradition, both in ensembles (from chamber music to orchestras) and as solo instruments. Violins are also important in many varieties of folk music, including country music, bluegrass music, and in jazz. Electric violins with solid bodies and piezoelectric pickups are used in some forms of rock music and jazz fusion, with the pickups plugged into instrument amplifiers and speakers to produce sound.\n");
                break;
            case ("flute"):
                imgView.setImageResource(R.drawable.flute);
                description.setText("The flute is a woodwind instrument, but modern flutes are made of metal. It was made of wood a long time ago. It doesn't need lip vibration like brass instruments. Flute players hold it horizontally and make a sound by blowing their breath over the edge of the hole of flute's head. This makes the air vibrate. Flutes need the second most amount of air for an instrument, next to the tuba. Flautists can change the pitch by pushing buttons or changing the direction of the breath. These buttons are called keys. The breath also can change the tone or the volume. Flute has many variations of the sound. The sound of flute is high, so composers often express it as a bird. A flute matches with many instruments such as violin and piano, and is often part of an ensemble.");
                break;
            case ("reading"):
                imgView.setImageResource(R.drawable.reading);
                description.setText("Reading is the process of taking in the sense or meaning of letters, symbols, etc., especially by sight or touch. Reading is typically an individual activity, done silently, although on occasion a person reads out loud for other listeners; or reads aloud for one's own use, for better comprehension.\n" +
                        "As a leisure activity, children and adults read because it is pleasant and interesting. In the US, about half of all adults read one or more books for pleasure each year. About 5% read more than 50 books per year. Americans read more if they: have more education, read fluently and easily, are female, live in cities, and have higher socioeconomic status. Children become better readers when they know more about the world in general, and when they perceive reading as fun rather than another chore to be performed.\n" +
                        "Reading for pleasure has been linked to increased cognitive progress in vocabulary and mathematics during adolescence. Sustained high volume lifetime reading has been associated with high levels of academic attainment.\n" +
                        "Reading has also been shown to improve stress management, memory, focus, writing skills, and imagination. The cognitive benefits of reading continue into mid-life and the senior years. Reading books and writing are among brain-stimulating activities shown to slow down cognitive decline in seniors.\n");
                break;
            case ("microscopy"):
                imgView.setImageResource(R.drawable.microscopy);
                description.setText("Amateur Microscopy is the investigation and observation of biological and non-biological specimens for recreational purposes. Collectors of minerals, insects, seashells, and plants may use microscopes as tools to uncover features that help them classify their collected items. Other amateurs may be interested in observing the life found in pond water and of other samples. Microscopes may also prove useful for the water quality assessment for people that keep a home aquarium. Photographic documentation and drawing of the microscopic images are additional tasks that augment the spectrum of tasks of the amateur. There are even competitions for photomicrograph art. Participants of this pastime may either use commercially prepared microscopic slides or engage in the task of specimen preparation.");
                break;
            case ("audio"):
                imgView.setImageResource(R.drawable.audio);
                description.setText("Music is a form of art that uses sound organised in time. Music is also a form of entertainment that puts sounds together in a way that people like, find interesting or dance to. Most music includes people singing with their voices or playing musical instruments, such as the piano, guitar, drums or violin.\n" +
                        "People can enjoy music by listening to it. They can go to concerts to hear musicians perform. Classical music is usually performed in concert halls, but sometimes huge festivals are organized in which it is performed outside, in a field or stadium, like pop festivals. People can listen to music on CD's, Computers, iPods, television, the radio, casette/record-players and even mobile phones.\n" +
                        "An audiophile is a person who cares about how good music sounds on a stereo or other high fidelity sound system. An audiophile is interested in how to improve the sound quality of sound recordings, such as vinyl records and Compact Discs, as well as the equipment used to listen to recordings such as CD players, amplifiers and loudspeakers.\n");
                break;
            case ("physics"):
                imgView.setImageResource(R.drawable.physics);
                description.setText("Physics is a branch of science. It is one of the most fundamental scientific disciplines. The main goal of physics is to explain how things move in space and time and understand how the universe behaves. It studies matter, forces and their effects.\n" +
                        "The word physics comes from the Greek word ἡ φύσις, meaning \"nature\". Physics can also be defined as \"that department of knowledge which relates to the order of nature, or, in other words, to the regular succession of events\".\n" +
                        "Astronomy, a part of physics, is the oldest natural science. In the past it was a part of 'natural philosophy' with other fields of science, such as chemistry and biology. During the scientific revolution, these fields became separate, and physics became a distinct field of knowledge.\n" +
                        "Physics is very important in the development of new technologies, such as airplanes, televisions, computers and nuclear weapons. Mechanics, a branch of physics, helped develop the mathematical field of calculus.\n" +
                        "Modern physics connects ideas about the four laws of symmetry and conservation of energy, momentum, charge, and parity.\n");
                break;
            case ("languages"):
                imgView.setImageResource(R.drawable.languages);
                description.setText("Language is the normal way humans communicate. Only humans use language, though other animals communicate through other means. The study of language is called linguistics.\n" +
                        "Second language refers to any language learned in addition to a person's first language; although the concept is named second-language acquisition, it can also incorporate the learning of third, fourth, or subsequent languages. Second-language acquisition refers to what learners do; it does not refer to practices in language teaching, although teaching can affect acquisition. The term acquisition was originally used to emphasize the non-conscious nature of the learning process,  but in recent years learning and acquisition have become largely synonymous.\n");
                break;
            case ("chemistry"):
                imgView.setImageResource(R.drawable.chemistry);
                description.setText("Chemistry is the scientific study of the properties and behavior of matter. It is a natural science that covers the elements that make up matter to the compounds composed of atoms, molecules and ions: their composition, structure, properties, behavior and the changes they undergo during a reaction with other substances.\n" +
                        "Amateur chemistry or home chemistry is the pursuit of chemistry as a private hobby. Amateur chemistry is usually done with whatever chemicals are available at disposal at the privacy of one's home.\n" +
                        "Throughout much of the 20th century, amateur chemistry was an unexceptional hobby, with high-quality chemistry sets readily available, and laboratory suppliers freely selling to hobbyists. For example, Linus Pauling had no difficulty in procuring potassium cyanide at the age of eleven. However, due to increasing concerns about terrorism, drugs, and safety, suppliers became increasingly reluctant to sell to amateurs, and chemistry sets were steadily toned down. This trend has gradually continued, leaving hobbyists in many parts of the world without access to most reagents.\n");
                break;
            case ("astronomy"):
                imgView.setImageResource(R.drawable.astronomy);
                description.setText("Astronomy (from Greek: ἀστρονομία, literally meaning the science that studies the laws of the stars) is a natural science that studies celestial objects and phenomena.\n" +
                        "Amateur astronomy is a hobby where participants enjoy observing or imaging celestial objects in the sky using the unaided eye, binoculars, or telescopes. Even though scientific research may not be their primary goal, some amateur astronomers make contributions in doing citizen science, such as by monitoring variable stars, double stars, sunspots, or occultations of stars by the Moon or asteroids, or by discovering transient astronomical events, such as comets, galactic novae or supernovae in other galaxies.\n" +
                        "Amateur astronomers use a range of instruments to study the sky, depending on a combination of their interests and resources. Methods include simply looking at the night sky with the naked eye, using binoculars, and using a variety of optical telescopes of varying power and quality, as well as additional sophisticated equipment, such as cameras, to study light from the sky in both the visual and non-visual parts of the spectrum. Commercial telescopes are available, new and used, but it is also common for amateur astronomers to build (or commission the building of) their own custom telescopes. Some people even focus on amateur telescope making as their primary interest within the hobby of amateur astronomy.\n");
                break;
            case ("geography"):
                imgView.setImageResource(R.drawable.geography);
                description.setText("Geography is often defined in terms of two branches: human geography and physical geography. Human geography is concerned with the study of people and their communities, cultures, economies, and interactions with the environment by studying their relations with and across space and place. Physical geography is concerned with the study of processes and patterns in the natural environment like the atmosphere, hydrosphere, biosphere, and geosphere.");
                break;
            case ("history"):
                imgView.setImageResource(R.drawable.history);
                description.setText("History is the study of past events. People know what happened in the past by looking at things from the past including sources (like books, newspapers, and letters) and artifacts (like pottery, tools, and human or animal remains.) Libraries, archives, and museums collect and keep these things for people to study history. A person who studies history is called a historian. A person who studies pre-history and history through things left behind by ancient cultures is called an archaeologist. A person who studies mankind and society is called an anthropologist. The study of the sources and methods used to study and write history is called historiography.\n" +
                        "People can learn about the past by talking to people who remember things that happened at some point in the past. This is called oral history. For example, when people who had been slaves and American Civil War survivors got old, some historians recorded them talking about their lives, so that history would not be lost.\n");
                break;
            case ("psychology"):
                imgView.setImageResource(R.drawable.psychology);
                description.setText("Psychology is the study of the mind. The study also covers thoughts, feelings, and behaviors. It also falls under the academic domain; some parts of psychology follow the way of doing science: explains the mind, how it works, and what it shows through our actions.\n" +
                        "When doing psychology, it has to deal with humans most of the time but animals beyond humans at some point. People, experts or not, can't study psychology as a whole. So, we divide small parts of it one step at a time. Also, psychology has much in common with other fields that they overlap with one another. Some of these fields fall in natural sciences or not.\n" +
                        "Psychologists explore behavior and mental processes, including perception, cognition, attention, emotion, intelligence, subjective experiences, motivation, brain functioning, and personality. Psychologists' interests extend to interpersonal relationships, psychological resilience, family resilience, and other areas within social psychology. Psychologists also consider the unconscious mind. Research psychologists employ empirical methods to infer causal and correlational relationships between psychosocial variables. \n");
                break;
        }
    }
}