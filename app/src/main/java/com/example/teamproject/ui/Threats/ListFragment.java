package com.example.teamproject.ui.Threats;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teamproject.R;

public class ListFragment extends Fragment {

    ListView threats_list;
    String mData[] = {"low self-esteem", "nausea", "dizziness", "sense of boredom",
            "feeling alone", "anxiety", "sense of fear", "stomachache", "sweating",
            "depression", "car crash", "poor eyesight", "curvature of the spine", "insomnia",
            "anorexia", "obesity", "nervousness", "unhappiness", "lack of focus", "aggression", "addiction"};
    int mImages[] = {R.drawable.low_self_esteem, R.drawable.nausea, R.drawable.dizziness, R.drawable.sense_of_boredom,
            R.drawable.feelinf_alone, R.drawable.anxiety, R.drawable.sense_of_fear, R.drawable.stomachache, R.drawable.sweating,
            R.drawable.depression, R.drawable.car_crash, R.drawable.poor_eyesight, R.drawable.curvature_of_the_spine, R.drawable.insomnia,
            R.drawable.anorexia, R.drawable.obesity, R.drawable.nervousness, R.drawable.unhappiness, R.drawable.lack_of_focus, R.drawable.aggression, R.drawable.addictions};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_fragment, container, false);

        threats_list = (ListView) view.findViewById(R.id.threats_list);

        MyAdapter adapter = new MyAdapter(getActivity(), mData, mImages);
        threats_list.setAdapter(adapter);

        threats_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position){
                    case 0:
                        //Toast.makeText(getActivity(), "content of threat 1", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        //Toast.makeText(getActivity(), "content of threat 2", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        //Toast.makeText(getActivity(), "content of threat 3", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        //Toast.makeText(getActivity(), "content of threat 4", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
        return view;
    }
    class MyAdapter extends ArrayAdapter<String>{

        Context context;
        String tTitles[];
        int tImages[];

        MyAdapter(Context context, String title[], int images[]) {
            super(context, R.layout.threats_item, title);
            this.context = context;
            this.tTitles = title;
            this.tImages = images;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View threats = layoutInflater.inflate(R.layout.threats_item, parent, false);

            ImageView images = threats.findViewById(R.id.threat_image);
            TextView title = threats.findViewById(R.id.threat_title);

            images.setImageResource(tImages[position]);
            title.setText(tTitles[position]);

            return threats;
        }
    }
}