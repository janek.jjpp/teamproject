package com.example.teamproject.ui.Suggestions;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.teamproject.R;
import com.example.teamproject.ui.Activity.ActivityDetails;
import com.example.teamproject.ui.Activity.Categories.FavouritesActivities;
import com.example.teamproject.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class SetSuggestions extends Fragment {

    FavouritesActivities favouritesActivities;
    Button reload;
    Button manage;
    TextView text_weekly;
    TextView text_monthly;
    public String caloriesCycling = (HomeFragment.average_monthly_minutes * 6) + " ";
    public String caloriesSwimming = (HomeFragment.average_monthly_minutes * 7) + " ";
    public String caloriesRunning = (HomeFragment.average_monthly_minutes * 12) + " ";
    public List<String> random_data_weekly = new ArrayList<String>(Arrays.asList("In this time you could learn a couple of openings in chess! Maybe a new one would give you the edge in the next game?",
            "That’s time you could dedicate to practice scales on the piano. Is there a better way to improve your improvisation skills?", "If you were learning words of a new language in this time, maybe they would help in a situation with a stranger abroad?"));
    public List<String> random_data_monthly = new ArrayList<String>(Arrays.asList("In this time you could have burnt "+caloriesCycling+ "calories by cycling. Your legs would be grateful for such a workout!",
            "In this time you could have burnt around " +caloriesSwimming + "calories by swimming. That's how you build a dream body!", "In this time you could have burnt around " +caloriesRunning+ "calories by running. Imagine how much more fit you would be!"));

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set_suggestions, container, false);

        //favouritesActivities = new FavouritesActivities();
        FavouritesSuggestions favouritesSuggestions = new FavouritesSuggestions();

        reload = view.findViewById(R.id.btn_reload);
        manage = view.findViewById(R.id.btn_manage);
        //manage.setVisibility(View.INVISIBLE);
        text_weekly = view.findViewById(R.id.text_weekly);
        text_monthly = view.findViewById(R.id.text_monthly);

        set();

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(ActivityDetails.favourites.isEmpty()){
                    set();
                //}
                //else{
                    //take data from favourites
                //}
            }
        });
        manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(favouritesSuggestions);
            }
        });
        return view;
    }

    public void set(){
        final int i = new Random().nextInt(random_data_weekly.size());
        final int j = new Random().nextInt(random_data_monthly.size());

        String weekly = ((HomeFragment.average_weekly_minutes / 60) + "h " + (HomeFragment.average_weekly_minutes % 60) + "min");
        String monthly = ((HomeFragment.average_monthly_minutes / 60) + "h " + (HomeFragment.average_monthly_minutes % 60) + "min");

        //text_weekly.setText("Weekly\n\nYour total time spend on the phone this week is "+ weekly + "\n\nThat's enough to learn \n"+random_data.get(i));
        //text_monthly.setText("Monthly\n\nYour total time spend on the phone this month is "+ monthly + "\n\nThat's enough to learn \n"+random_data.get(j));
        text_weekly.setText("You’ve spent "+weekly+" on your phone this week. "+random_data_weekly.get(i));
        text_monthly.setText("You’ve spent "+monthly+" on your phone this month. "+random_data_monthly.get(j));
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_suggestions, fragment);
        fragmentTransaction.commit();
    }
}