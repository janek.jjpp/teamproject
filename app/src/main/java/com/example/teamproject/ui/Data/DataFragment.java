package com.example.teamproject.ui.Data;

import android.accessibilityservice.GestureDescription;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teamproject.MainActivity;
import com.example.teamproject.R;
import com.example.teamproject.ui.home.HomeFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.System.exit;

public class DataFragment extends Fragment {

    int counter = 0;
    String tempTitles[] = new String[1000];
    String tempData[] = new String[1000];
    int tempImages[] = new int[1000];

    String Titles[];
    String Data[];
    int Images[];
    String appName;
    ListView applications;
    public static Integer generalHours = 0;
    public static Integer generalMinutes = 0;

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.data_fragment, container, false);
        generalHours = 0;
        generalMinutes = 0;
        installedApps();
        applications = (ListView) view.findViewById(R.id.app_list);
        Titles = new String[counter];
        Data = new String[counter];
        Images = new int[counter];
        for(int i = 0; i < counter; i++){
            Titles[i] = tempTitles[i];
            Data[i] = tempData[i];
            Images[i] = tempImages[i];
        }
        MyAdapter adapter = new MyAdapter(getActivity(), Titles, Data, Images);
        applications.setAdapter(adapter);

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                Toast.makeText(getContext(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                if (doubleBackToExitPressedOnce)
                    exit(1);
                else
                    doubleBackToExitPressedOnce = true;
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        return view;
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitles[];
        String rData[];
        int rImages[];

        MyAdapter(Context context, String title[], String data[], int images[]) {
            super(context, R.layout.data_item, title);
            this.context = context;
            this.rTitles = title;
            this.rData = data;
            this.rImages = images;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View applications = layoutInflater.inflate(R.layout.data_item, parent, false);

            ImageView images = applications.findViewById(R.id.app_image);
            TextView title = applications.findViewById(R.id.app);
            TextView data = applications.findViewById(R.id.app_data);

            images.setImageResource(rImages[position]);
            title.setText(rTitles[position]);
            data.setText(rData[position]);

            return applications;
        }
    }
    public int installedApps(){

        List<PackageInfo> apps = getActivity().getPackageManager().getInstalledPackages(0);
        for(int i = 0; i < apps.size(); i++){
            PackageInfo packageInfo = apps.get(i);
            if((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0){
                appName = packageInfo.applicationInfo.loadLabel(getActivity().getPackageManager()).toString();
                Log.e("App Name:" + Integer.toString(i), appName);
                tempTitles[counter] = appName;
                Random minutes = new Random();
                Random hours = new Random();
                Integer minutesVal;
                Integer hoursVal;
                minutesVal = minutes.nextInt(59 - 0) + 0;
                hoursVal = hours.nextInt(10 - 0)+0; //depends on amount of apps
                tempData[counter] = String.valueOf(hoursVal) + "h " + String.valueOf(minutesVal) + "min";
                generalHours += hoursVal;
                generalMinutes += minutesVal;
                //tempData[counter] = 0 + "";
                switch (appName) {
                    case "Facebook":
                        tempImages[counter] = R.drawable.icon_facebook;
                        break;
                    case "TeamProject":
                        tempImages[counter] = packageInfo.applicationInfo.icon;
                        break;
                    case "Messenger":
                        tempImages[counter] = R.drawable.icon_messenger;
                        break;
                    case "Snapchat":
                        tempImages[counter] = R.drawable.icon_snap;
                        break;
                    case "Youtube":
                        tempImages[counter] = R.drawable.icon_yt;
                        break;
                    case "Twitch":
                        tempImages[counter] = R.drawable.icon_twitch;
                        break;
                    case "Netflix":
                        tempImages[counter] = R.drawable.icon_netflix;
                        break;
                    case "Chrome":
                        tempImages[counter] = R.drawable.icon_chrome;
                        break;
                    case "Instagram":
                        tempImages[counter] = R.drawable.icon_instagram;
                        break;
                    case "Firefox":
                        tempImages[counter] = R.drawable.icon_firefox;
                        break;
                    case "Allegro":
                        tempImages[counter] = R.drawable.icon_allegro;
                        break;
                    case "Yahoo Mail":
                        tempImages[counter] = R.drawable.icon_yahoo;
                        break;
                    case "OLX.pl":
                        tempImages[counter] = R.drawable.icon_olx;
                        break;
                    case "GitHub":
                        tempImages[counter] = R.drawable.icon_git;
                        break;
                    case "McDonald's":
                        tempImages[counter] = R.drawable.icon_mcdonalds;
                        break;
                    case "Vimeo":
                        tempImages[counter] = R.drawable.icon_vimeo;
                        break;
                    case "HBO GO":
                        tempImages[counter] = R.drawable.icon_hbogo;
                        break;
                    default:
                        tempImages[counter] = R.drawable.icon_question;
                        break;
                }

                System.out.println("app name - " + appName);
//                else
//                    tempImages[counter] = packageInfo.applicationInfo.icon;
                counter++;
            }
        }
        return counter;
    }
}